package lectures.part1basics

object ValuesVariablesTypes extends App {

  val x = 42
  //COMPILER can infer types -> val x: Int = 42
  println(x)


  //VALS ARE IMMUTABLE

  val aString: String = "hello"
  val anotherString = "goodbye"

  val aBoolean: Boolean = false
  val aChar: Char = 'a'
  val anInt: Int = x
  val aShort: Short = 4613
  val aLong: Long = 5211234123123432L
  val aFloat: Float = 2.0f
  val aDouble: Double = 3.14

  //variables
  var aVariable: Int = 4
//mutable and have side effects
//prefer vals over vars
  //all vals and vars have types
  //
}
