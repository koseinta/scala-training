package lectures.part1basics

import scala.annotation.tailrec

object Functions extends App {

  def aFunction(a: String, b: Int): String = {
    a + " " + b
  }
  println(aFunction("hello", 3))

  def aParameterlessFunction(): Int = 42
  println(aParameterlessFunction())
  println(aParameterlessFunction) //a parameterless function can be called without the ()

  def aRepteatedFunction(aString: String, n: Int): String = {
    if (n == 1) aString
    else aString + aRepteatedFunction(aString, n-1)

  }
  println(aRepteatedFunction("hello",3))
            //  ~~~~~~~~~~~~~~~~~~~WHEN YOU NEED LOOPS, USE RECURSION~~~~~~~~~ok now you know 50% of scala

  def aFunctionWithSideEffects(aString: String): Unit = println(aString)


  def aBigFunction(n: Int): Int = {
    def aSmallerFunction(a: Int, b: Int): Int = a + b

    aSmallerFunction(n, n-1)
  }

//Exercise 1:  A greeting function (name, age) -> "Hi, my name is $name and I am $age years old."

  def aGreetingFunction(name: String, age: Int): String = s"Hi, my name is $name and I am $age years old."

  println(aGreetingFunction("kostas", 31))

  //Exercise 2: Factorial function 1 * 2 * 3 * .. * n

  def aFactorialFunction(n: Int): Long = {
    if (n == 1) n
    else n * aFactorialFunction(n-1)
  }
  println(aFactorialFunction(7))

  def anotherFactorial(n: Int): BigInt = {
    @tailrec //USE this annotation if you want the compiler to warn you if your function is not tail recursion
    def factHelper(x: Int, accumulator: BigInt): BigInt =
      if (x <= 1) accumulator
      else factHelper(x-1,x*accumulator) //TAIL RECURSION = use recursive call as the LAST expression

    factHelper(n,1)
  }
  println(anotherFactorial(100))
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~WHEN YOU NEED LOOPS, USE TAIL RECURSION~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  //Exercise 5: Concatenate a string n times
  def nTimesStringConcatenation(n: Int, s: String): String = {
    @tailrec
    def funcHelper(x: Int, s: String, accumulator: String): String =
      if (x <= 1) accumulator
      else funcHelper(x-1,s,accumulator + s) //TAIL RECURSION = use recursive call as the LAST expression

    funcHelper(n, s, s)
  }
  println(nTimesStringConcatenation(5, "consciousness"))

  @tailrec
  def nTimesStringConcatenation2(n: Int, s: String): String = {
      val accumulator = s
      if (n <= 1) accumulator
      else nTimesStringConcatenation2(n-1,accumulator + s) //TAIL RECURSION = use recursive call as the LAST expression

  }
  println(nTimesStringConcatenation(5, "consciousness"))

  //Exercise 6: IsPrime function tail recursive

  //Exercise 7: Fibonacci function tail recursive


  //Exercise 3: A fibonacci function f(1) = 1, f(2) = 1, f(n) = f(n-1)+f(n-2)

  //f(3) = 2, f(4) = 3, f(5) = 5, f(6) = 8

  def aFibonacciFunction(n: Int): Long = {
    if (n == 2 || n == 1) 1
    else aFibonacciFunction(n-2) + aFibonacciFunction(n-1)
  }
  println(aFibonacciFunction(9))

  //Exercise 4: Test if a number is prime

  //1, 2, 3, 5, 7, 11, 13, 17, ...

  def isPrime(n: Int): Boolean = {
    def isPrimeUntil(t: Int): Boolean =
      if (t <= 1) true
      else n % t != 0 && isPrimeUntil(t-1)

    isPrimeUntil(n / 2)
  }
  println(isPrime(37))
  println(isPrime(2003))
  println(isPrime(37 * 17))
//test commit

}

